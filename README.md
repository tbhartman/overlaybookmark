Overlay Bookmark
================

Goal
----

Provide an easy way to overlay an image on top of a webpage and interact with
the underlying site.

Installation
------------

 * clone repo
 * `npm install -g uglify-js-es6`
 * `make`
 * manually copy contents of `overlay_bookmark.txt` into location field of a bookmark

Usage
-----

 * click bookmarklet
 * focus on text input element
 * paste an image
 * click "clear" (upper right corner) to remove the resulting div

ToDo
----

 * ability to move image
 * ability to adjust opacity
 * ability to zoom image
 * make it prettier
