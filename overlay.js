(()=>{
    const div_class_id = 'c2e88a86-a3b8-4d55-8d98-dce8484dec54';
    const div_class = 'overlay_bookmark';
    let div = document.body.querySelector(['div', div_class, div_class_id].join('.'));
    if (div === null) {
        div = document.createElement('div');
        div.classList.add(div_class);
        div.classList.add(div_class_id);
        document.body.appendChild(div);
    }
    while (div.childNodes.length) {
        div.removeChild(div.childNodes[0]);
    }
    div.style.zIndex = 99999;
    div.style.width = '98vw';
    div.style.height = '98vh';
    div.style.top = '1vh';
    div.style.left = '1vw';
    div.style.position = 'fixed';
    div.style.opacity = 0.5;
    div.style.pointerEvents = 'none';
    let button = document.createElement('button');
    button.textContent = 'hello';
    button.style.left = '0';
    button.style.top = '0';
    button.style.width = '100%';
    button.style.height = '100%';
    button.style.position = 'relative';
    button.style.pointerEvents = 'none';
    div.appendChild(button);
    let clear = document.createElement('button');
    clear.textContent = 'clear';
    clear.addEventListener('click', ()=>{
        document.body.removeChild(div);
    });
    clear.style.position = 'absolute';
    clear.style.right = 0;
    clear.style.top = 0;
    clear.style.pointerEvents = 'auto';
    div.appendChild(clear);
    let ok = document.createElement('button');
    ok.textContent = 'ok';
    ok.addEventListener('click', ()=>{
        div.removeChild(button);
        div.removeChild(ok);
    });
    ok.style.position = 'absolute';
    ok.style.left = 0;
    ok.style.top = 0;
    ok.style.pointerEvents = 'auto';
    div.appendChild(ok);
    let text = document.createElement('input');
    text.addEventListener('paste', (e)=>{
        console.log('paste');
        for (i in e.clipboardData.items) {
            let item = e.clipboardData.items[i];
            if (item.kind === 'file') {
                let blob = item.getAsFile();
                let reader = new FileReader();
                reader.onload = (e)=>{
                    let img = document.createElement('img');
                    img.src = e.target.result;
                    img.style.position = 'absolute';
                    img.style.left = '10px';
                    img.style.top = '20px';
                    div.appendChild(img);
                    console.log(e.target);
                };
                reader.readAsDataURL(blob);
                break;
            }
        }
    });
    text.style.position = 'absolute';
    text.style.top = '20vh';
    text.style.left = '20vw';
    text.style.pointerEvents = 'auto';
    div.appendChild(text);
})()
