all: overlay_bookmark.txt

overlay_bookmark.txt: uglified.js
	echo "javascript:" | tr -d "\n" > $@
	cat $< >> $@

uglified.js: overlay.js
	uglifyjs --compress --mangle -- $< > uglified.js
